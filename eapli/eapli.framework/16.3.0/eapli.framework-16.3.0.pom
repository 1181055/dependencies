<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<packaging>pom</packaging>
	<groupId>eapli</groupId>
	<version>16.3.0</version>
	<name>eapli.framework</name>
	<artifactId>eapli.framework</artifactId>


	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.4.2</version>
	</parent>


	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

		<java.version>11</java.version>
		<maven.compiler.source>11</maven.compiler.source>
		<maven.compiler.target>11</maven.compiler.target>

		<sonar.jacoco.reportPaths>target/jacoco.exec</sonar.jacoco.reportPaths>
		<sonar.exclusions>src/main/java/**/*/package-info.java</sonar.exclusions>

		<downloadSources>true</downloadSources>
		<downloadJavadocs>true</downloadJavadocs>
	</properties>


	<modules>
		<module>eapli.framework.core</module>
		<module>eapli.framework.infrastructure.authz</module>
		<module>eapli.framework.infrastructure.pubsub</module>
	</modules>


	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-json</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
		<!-- keep junit4 for now -->
        <dependency>
            <groupId>org.junit.vintage</groupId>
            <artifactId>junit-vintage-engine</artifactId>
            <version>5.7.0</version>
            <scope>test</scope>
        </dependency>
        
		<!-- Apache Commons Lang 3 -->
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>3.11</version>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.8.0</version>
				<configuration>
					<release>11</release>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>versions-maven-plugin</artifactId>
				<version>2.5</version>
				<configuration>
					<generateBackupPoms>false</generateBackupPoms>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>3.2.0</version>
				<configuration>
					<!-- UML Taglet JDK 8 -->
					<!-- <source>1.8</source> <javadocVersion>1.8.0</javadocVersion> <taglet>org.jdrupes.taglets.plantUml.Taglet</taglet> 
						<tagletArtifact> <groupId>org.jdrupes.taglets</groupId> <artifactId>plantuml-taglet</artifactId> 
						<version>1.0.5</version> </tagletArtifact> -->

					<!-- UML DocLet JDK 9+ -->
					<doclet>nl.talsmasoftware.umldoclet.UMLDoclet</doclet>
					<docletArtifact>
						<groupId>nl.talsmasoftware</groupId>
						<artifactId>umldoclet</artifactId>
						<version>2.0.12</version>
					</docletArtifact>

					<!-- params -->
					<doclint>none</doclint>
					<additionalOptions>
						<additionalOption>-Xdoclint:none</additionalOption>
						<!-- Specify each diagram option here as an additionOption tag. -->
					</additionalOptions>
					<additionalJOption>-Xdoclint:none</additionalJOption>
				</configuration>
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<executions>
					<execution>
						<id>attach-sources</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>

	<distributionManagement>
		<repository>
			<id>bintray-pagsousa-eapli</id>
			<name>pagsousa-eapli</name>
			<url>https://api.bintray.com/maven/pagsousa/eapli/eapli.framework/;publish=1</url>
		</repository>
	</distributionManagement>

	<repositories>
		<repository>
			<id>jitpack.io</id>
			<url>https://jitpack.io</url>
		</repository>
	</repositories>
</project>